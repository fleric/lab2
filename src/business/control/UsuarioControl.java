package business.control;

import java.util.ArrayList;

import business.model.Usuario;

public class UsuarioControl {
	
	ArrayList<Usuario> usuarioList = new ArrayList<>();

	public void addUsuario(Usuario usuario) throws InvalidUsuarioException {
		usuario.setLogin(usuario.getLogin().replaceAll(" ", ""));
		usuario.setSenha(usuario.getSenha().replaceAll(" ", ""));
		
		if(usuario.getLogin().isEmpty() 
				|| usuario.getLogin().length() >= 20 
				|| usuario.getLogin().matches(".*\\d.*")) {
			throw new InvalidUsuarioException("erro login");
		}
		
		if(usuario.getSenha().length() < 8 
				|| usuario.getSenha().length() > 12
				|| !possuiNumero(usuario.getSenha())) {
			throw new InvalidUsuarioException("erro senha");
		}
		
		if(usuarioList.contains(usuario)){
			System.out.println("erro duplicado");
//			throw new NOVAEXCECAO 
		}
		
		//chamar persistencia
		usuarioList.add(usuario);
	}
	
	private boolean possuiNumero(String s){
	    int numbers = 0;
	    
	    for(char c : s.toCharArray())
	    {
	        if(Character.isDigit(c))
	            ++numbers;
	    }
	    
	    if(numbers < 2)
	        return false;
	    
	    return true;
	}
}
